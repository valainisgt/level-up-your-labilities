param()

$switchName      = 'presentation'
$dcIPAddress     = '192.168.10.2'
$dbIPAddress     = '192.168.10.3'
$webIPAddress    = '192.168.10.4'
$smbIPAddress    = '192.168.10.5'
$cliIPAddress    = '192.168.10.6'

$password         = 'Pass@word123'
$domainName       = 'gspdg.test.lab'

return @{
    AllNodes = @(
        @{
            NodeName = '*'

            Lability_ProcessorCount = 2;
            Lability_MaximumMemory  = 4GB;
            Lability_SwitchName     = $switchName
            Lability_Media          = '2022_x64_Standard_EN_Eval';

            InterfaceAlias   = 'Ethernet'
            AddressFamily    = 'IPv4'
            PrefixLength     = '24'
            DnsServerAddress = $dcIPAddress

            PSDscAllowDomainUser        = $true
            PSDscAllowPlainTextPassword = $true
        }
        @{
            NodeName         = 'DC'
            IPAddress        = $dcIPAddress
            DnsServerAddress = '127.0.0.1'
            Roles            = @('DC')
        }
        @{
            NodeName          = 'DB'
            IPAddress         = $dbIPAddress
            Roles             = @('MS', 'DB')
            Lability_Resource = @('sqlserver')
        }
        @{
            NodeName          = 'WEB'
            IPAddress         = $webIPAddress
            Roles             = @('MS', 'WEB')
            Lability_Resource = @('aspdotnet7runtime')
        }
        @{
            NodeName  = 'SMB'
            IPAddress = $smbIPAddress
            Roles     = @('MS', 'SMB')

            Disks = @(
                @{
                    DiskNumber  = 1
                    DriveLetter = 'I'
                    FSFormat    = 'NTFS'
                    SmbShareName = 'Share1'
                }
            )

            Lability_HardDiskDrive = @(
                @{
                    Generation       = 'VHDX'
                    MaximumSizeBytes = 256GB
                }
            )

        }
        @{
            NodeName       = 'CLI'
            IPAddress      = $cliIPAddress
            Roles          = @('MS', 'CLI')
            Lability_Media = 'WIN10_x64_Enterprise_22H2_EN_Eval'
        }
    )
    NonNodeData = @{
        Password    = $password
        Username    = 'Administrator'
        DomainName  = $domainName

        Lability = @{
            Resource = @(
                @{
                    Id       = 'sqlserver';
                    Filename = 'SQLServer2019-x64-ENU-Dev.iso';
                    Uri      = 'https://download.microsoft.com/download/7/c/1/7c14e92e-bdcb-4f89-b7cf-93543e7112d1/SQLServer2019-x64-ENU-Dev.iso'
                    Expand   = $true
                }
                @{
                    Id       = 'aspdotnet7runtime'
                    FileName = 'aspdotnet7runtime.exe'
                    Uri      = 'https://download.visualstudio.microsoft.com/download/pr/ff197e9e-44ac-40af-8ba7-267d92e9e4fa/d24439192bc549b42f9fcb71ecb005c0/dotnet-hosting-7.0.3-win.exe'
                }
            )
            DSCResource = @(
                @{ Name = 'ComputerManagementDsc'; }
                @{ Name = 'NetworkingDsc'; }
                @{ Name = 'PSDscResources'; }
                @{ Name = 'ActiveDirectoryDsc'; }
                @{ Name = 'SqlServerDsc'; }
                @{ Name = 'StorageDsc'; }
            )
            Network = @(
                @{ Name = $switchName; Type = 'Internal'; }
            )
        }
    }
}