Configuration HostConfiguration {

    param(
        [ValidateNotNullOrEmpty()]
        [string]
        $VirtualHardDiskPath = 'c:\hyperv\Virtual Hard Disks',

        [ValidateNotNullOrEmpty()]
        [string]
        $VirtualMachinePath = 'c:\hyperv\Virtual Machines',

        [ValidateNotNullOrEmpty()]
        [string]
        $LabilityPath = 'c:\lability'
    )
    
    Import-DscResource -ModuleName PsDscResources
    Import-DscResource -ModuleName xHyper-V
    Import-DscResource -ModuleName NetworkingDsc
    Import-DscResource -ModuleName PowerShellGet -ModuleVersion 2.2.5
    
    node 'localhost' {

        Script DisableServerManager {
            GetScript = {
                return Get-ScheduledTask -TaskName Servermanager -ErrorAction SilentlyContinue
            }
            TestScript = {
                $x = Get-ScheduledTask -TaskName Servermanager -ErrorAction SilentlyContinue
                if($null -eq $x) { return $true }
                else {
                    return $x.State -eq 'Disabled'
                }
            }
            SetScript = {
                Get-ScheduledTask -TaskName Servermanager | Disable-ScheduledTask
            }
        }

        WindowsFeatureSet HyperV {
            Name                 = @('Hyper-V', 'RSAT-Hyper-V-Tools')
            Ensure               = 'Present'
            IncludeAllSubFeature = $true
        }

        xVMHost 'HostPaths' {
            IsSingleInstance    = 'Yes'
            VirtualHardDiskPath = $VirtualHardDiskPath
            VirtualMachinePath  = $VirtualMachinePath
            DependsOn = @('[WindowsFeatureSet]HyperV')
        }

        $switches = @(
            @{ Name = 'presentation'; IPAddress = '192.168.10.1' }
        )
        foreach($switch in $switches) {

            xVMSwitch "Create VM Switch $($switch.Name)" {
                DependsOn = $featureDependsOn
                Name      = $switch.Name
                Type      = 'internal'
            }

            IPAddress "Update VM Switch $($switch.Name) NIC" {
                DependsOn      = "[xVMSwitch]Create VM Switch $($switch.Name)"
                InterfaceAlias = "vEthernet ($($switch.Name))"
                AddressFamily  = 'IPv4'
                IPAddress      = "$($switch.IPAddress)/24"
            }

            $ipParts = $switch.IPAddress.Split('.')
            $trustedHost = "$($ipParts[0]).$($ipParts[1]).$($ipParts[2]).*"

            Script "Trust Remoting Host $trustedHost" {
                GetScript = {
                    return @{
                        Result = (Get-Item WSMan:localhost\client\TrustedHosts)
                    }
                }
                TestScript = {
                    $currentTrustedHosts = Get-Item WSMan:localhost\client\TrustedHosts
                    $currentTrustedHosts.Value.Split(',', [System.StringSplitOptions]::RemoveEmptyEntries) -contains $Using:trustedHost
                }
                SetScript = {
                    $currentTrustedHosts = Get-Item WSMan:localhost\client\TrustedHosts
                    $newTrustedHosts = $currentTrustedHosts.Value.Split(',', [System.StringSplitOptions]::RemoveEmptyEntries)
                    $newTrustedHosts += $Using:trustedHost
                    Set-Item WSMan:localhost\client\trustedhosts -value $($newTrustedHosts -join ',')
                }
            }
        }

        $requiredModules = @(
            'ComputerManagementDsc'
            'NetworkingDsc'
            'ActiveDirectoryDsc'
            'StorageDsc'
            'PsDscResources'
            'SqlServerDsc'
            'Lability'
        )
        foreach($module in $requiredModules) {

            PSModule "Install $module" {
                Name = $module
                SkipPublisherCheck = $true
                InstallationPolicy = 'Trusted'
                AllowClobber       = $true
            }
        }

        Script 'Configure Lability' {
            DependsOn = '[PSModule]Install Lability'
            GetScript = {
                Import-Module Lability
                $current = Get-LabHostConfiguration
                return @{ 'Result' = $current }
            }
            TestScript = {
                Import-Module Lability
                return Test-LabHostConfiguration -IgnorePendingReboot
            }
            SetScript = {
                Import-Module Lability

                $labilityConfig = @{
                    ConfigurationPath = (Join-Path -Path $Using:LabilityPath -ChildPath 'Configurations')
                    DifferencingVhdPath = (Join-Path -Path $Using:LabilityPath -ChildPath 'VMVirtualHardDisks')
                    HotfixPath = (Join-Path -Path $Using:LabilityPath -ChildPath 'Hotfixes')
                    IsoPath = (Join-Path -Path $Using:LabilityPath -ChildPath 'ISOs')
                    ParentVhdPath = (Join-Path -Path $Using:LabilityPath -ChildPath 'MasterVirtualHardDisks')
                    ResourcePath = (Join-Path -Path $Using:LabilityPath -ChildPath 'Resources')
                }

                Set-LabHostDefault @labilityConfig
                Start-LabHostConfiguration -IgnorePendingReboot
            }
        }
    }
}