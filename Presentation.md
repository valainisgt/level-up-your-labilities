---
marp: true
---

![bg w:400 right:35%](https://raw.githubusercontent.com/marp-team/marp/main/website/public/assets/marp-logo.svg)
# Level Up Your Labilities
## Infrastructure as Code and <br /> Configuration as Code with PowerShell

by Greg Valainis

[https://gitlab.com/valainisgt](https://gitlab.com/valainisgt)

---

# Configuration as Code
- Declarative code
- Works on my machine
- Backed by version control

---

# Configuration as Code with PowerShell
- PSDesiredStateConfiguration v1.1
- Management platform
- Ships with Windows PowerShell 5.1
- Primary Components
  - Local Configuration Manager
  - Configurations
  - Resources

---

# Local Configuration Manager
- Engine of Desired State Configuration
  1. Read configuration documents
  2. Determine if computer is in compliance
  3. Fix if necessary
- Can be tuned to your liking
  - Refresh mode (push or pull)
  - Refresh rate (>= 15 minutes)
  - Reboot if needed
  - Configuration Mode (ApplyOnly, ApplyAndMonitor, ApplyAndAutoCorrect)

---

# Push vs Pull
- Push - actively apply a configuration
- Pull - client gets configuration from a remote service
- Pull is the preferred solution

---

# DSC Configurations
- Just a PowerShell Script
- `Configuration` script block
- Import resource modules
- One or more `Node` script blocks
- One or more resource blocks
- Compiled to machine readable format

---

```ps1
Configuration 'MyDscConfiguration' {
    
    Import-DscResource -ModuleName PSDscResources
    
    Node @('DC1','DC2') {
        WindowsFeature 'DnsServer' {
            Name   = 'DNS'
            Ensure = 'Present'
        }
        WindowsFeature 'AD' {
            Name   = 'ADDS'
            Ensure = 'Present'
        }
    }

    Node 'MS1' {
        Timezone 'Eastern' {
            IsSingleInstance = 'Yes'
            Timezone         = 'Eastern Standard Time'
        }
    }
}
```

---

# DSC Resource
- Building block for a DSC Configuration
- Exposes properties that can be configured (schema)
- PowerShell script functions that 'make it so'
	1. Get
	2. Test
	3. Set
- Several common properties
  - `DependsOn`
  - `Ensure`
  - `PSRunAsCredential`

---

```ps1
Environment [string] $InstanceName {
    Name                   = [string]
    [ Path                 = [bool] ]
    [ Value                = [string] ]
    [ Ensure               = [string] { Present | Absent } ]
    [ DependsOn            = [string[]] ]
    [ PSRunAsCredential    = [PSCredential] ]
}
```

---

```ps1
Environment 'msbuild' {
    Name   = 'msbuild'
    Path   = $true
    Value  = 'C:\Program Files\...\MSBuild\Current\Bin'
    Ensure = 'Present'
}
```

---

# Dynamic Configurations
- It's just a PowerShell script
- Parameters
- Conditionals
- Loops
- Configuration Data

---

```ps1
Configuration 'MyDscConfiguration' {
    param (
        [string[]]$ComputerNames='localhost'
    )

    Node $ComputerNames {
        WindowsFeature 'MyFeatureInstance' {
            Ensure = 'Present'
            Name = 'RSAT'
        }

        WindowsFeature 'My2ndFeatureInstance' {
            Ensure = 'Present'
            Name = 'Bitlocker'
        }
    }
}

MyDscConfiguration -ComputerName 'localhost','Server01'
```

---

```ps1
Configuration 'MyDscConfiguration' {
    param (
        [bool]$UseWebServer=$true
    )

    Node 'localhost' {
	
        if ($UseWebServer) {

            WindowsFeature 'Install_IIS' {
                Ensure = 'Present'
                Name = 'Web-Server'
            }
        }
    }
}
```

---

```ps1
Configuration 'MyDscConfiguration' {

    Node 'localhost' {

        foreach ($service in $(Get-Service)) {
            
            Service $service.Name {
                Name      = $service.Name
                State     = $service.Status
                StartType = $service.StartType
            }
        }
    }
}
```
---
# Configuration Variance
- Separate configuration and environment data
- One `Configuration` for multiple environments
- Pass in `ConfigurationData` at compile time

---

# Configuration Data
- Just a hashtable
- At least one key `AllNodes`
- Zero or more other keys

---

# MyConfigurationData.psd1
```ps1
@{
    AllNodes =  @(
        @{
            NodeName   = '*'
            DnsAddress = '10.0.0.4'
        }
        @{
            NodeName = 'SERVER1'
            Role     = 'WebServer'
        }
        @{
            NodeName = 'SERVER2'
            Role     = 'SQLServer'
        }
    )

    # other keys
    ...
}

MyDscConfiguration -ConfigurationData $configData
```

---

```ps1
Configuration 'MyDscConfiguration' {

    Node $AllNodes.Where({ $_.Role -contains 'WebServer' }).NodeName {

        WindowsFeature 'Install_IIS' {
            Ensure = 'Present'
            Name   = 'Web-Server'
        }

        DnsServerAddress 'DNS' {
            Address = $node.DnsAddress
            ...
        }
    }
}
```

---

```ps1
Configuration 'MyDscConfiguration' {

    Node $AllNodes.NodeName {

        Computer 'JoinDomain' {
            Name       = $node.NodeName
            DomainName = $configurationData.NonNodeData.DomainName
            ...
        }
    }
}
```

---

# Custom Resources
- Extensive community driven resource modules
- Implement Get, Test, Set
  - One off `Script` resources
  - Custom PowerShell modules

---

# Script Resource

```ps1
Script 'CustomService' {
    GetScript = {
        return Get-Service $Using:ServiceName -ErrorAction 'SilentlyContinue'
    }
    TestScript = {
        $service = Get-Service $Using:ServiceName -ErrorAction 'SilentlyContinue'

        # determine if the service exists
        # and is in the correct state
    }
    SetScript = {
        $service = Get-Service $Using:ServiceName -ErrorAction 'SilentlyContinue'

        # create the service if necessary
        # Set startup type if necessary
        # Start/stop the service if necessary
    }
}
```

---

# DSC Usage
- Compile Configuration to MOF's
- Publish MOF to computer or pull servers
- Common commands
  - `Start-DscConfiguration`
  - `Get-DscConfigurationStatus`
  - `Set-DscLocalConfigurationManager`

---

# Future of PSDesiredStateConfiguration
- No longer a management platform
- Version 2.0.5
- Version 3.0.0-beta1

---

![bg w:400 right](https://learning.oreilly.com/library/cover/9781098114664/400w/)
# Infrastructure as Code
- Define Everything as Code
- Continuously test and deliver all work in progress
- Build small, simple pieces that can change independently

---

# Infrastructure Platforms
- Compute 
- Network
- Storage

---

![bg w:200 right:40%](https://openclipart.org/image/300px/svg_to_png/22734/papapishu-Lab-icon-1.png)
# Lability
- Uses DSC configuration data
- Provisions VMs with DSC configuration
- Targets Hyper-V platform
  - Virtual Machines
  - Virtual Networks
  - Virtual Machine Disk Image

[https://github.com/VirtualEngine/Lability](https://github.com/VirtualEngine/Lability)

---

# Compute
```ps1
@{
    AllNodes = @(

        @{
            Lability_StartupMemory  = ''
            Lability_MinimumMemory  = ''
            Lability_MaximumMemory  = ''
            Lability_ProcessorCount = ''
            Lability_Media          = ''
        }
    )
}
```

---

# Network
```ps1
@{
    NonNodeData = @{

        Lability = @{

            Network = @(

                @{
                    Name = 'CORPNET'
                    Type = 'Internal' # external, private
                }
            )
        }
    }

    AllNodes = @(
        @{
            NodeName            = '*'
            Lability_SwitchName = @('CORPNET')
        }
    )
}
```
---

# Storage
```ps1
@{
    AllNodes = @(

        @{
            NodeName = 'PC1'

            Lability_HardDiskDrive = @(
                
                @{
                    Generation        = 'VHDX'
                    MaximumSizeBytes = 150GB
                    Type              = 'Dynamic' # fixed
                }
            )
        }
    )
}
```

---

# External Files
```ps1
@{
    NonNodeData = @{

        Lability = @{

            Resource = @(

                @{
                    Id       = 'SQLServer2019'
                    Filename = 'SQLServer2019-Dev.iso'
                    Uri      = 'https://download.microsoft.com/.../sqlserver.iso'
                    Expand   = $true
                }
            )
        }
    }

    AllNodes = @(
        @{
            NodeName = 'SQL1'
            Lability_Resource = @('SQLServer2019')
        }
    )
}
```
---
# DSC Resources
```ps1
@{
    AllNodes = ...

    NonNodeData = @{
        
        Lability = @{
            
            DSCResource = @(

                @{ Name = 'ComputerManagementDsc' }

                @{ Name = 'NetworkingDsc' }

                @{
                    Name = 'CustomResourceModule'
                    Path = 'C:\My\Modules\CustomResourceModule'
                    Provider = 'FileSystem'
                }
            )
        }
    }
}
```

---

# Provisioning with Lability
- Compile DSC Configurations
- `Start-LabConfiguration`
- `Start-Lab` / `Wait-Lab`
