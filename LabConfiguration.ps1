Configuration LabConfiguration {

    Import-DscResource -ModuleName ComputerManagementDsc
    Import-DscResource -ModuleName NetworkingDsc
    Import-DscResource -ModuleName PSDscResources
    Import-DscResource -ModuleName ActiveDirectoryDsc
    Import-DscResource -ModuleName SqlServerDsc
    Import-DscResource -ModuleName StorageDsc

    $domainName             = $configurationData.NonNodeData.DomainName
    $plainTextPassword      = $configurationData.NonNodeData.Password
    $username               = $configurationData.NonNodeData.Username
    $password               = ConvertTo-SecureString -String $plainTextPassword -AsPlainText -Force
    $localUser              = New-Object System.Management.Automation.PSCredential ($username, $password)
    $domainuser             = New-Object System.Management.Automation.PSCredential ("$domainName\$($username)", $password)

    node $AllNodes.NodeName {
        LocalConfigurationManager {
            RebootNodeIfNeeded   = $true
            AllowModuleOverwrite = $true
            ConfigurationMode    = 'ApplyOnly'
        }
        
        Timezone Eastern {
            IsSingleInstance = 'Yes'
            Timezone         = 'Eastern Standard Time'
        }

        IPAddress PrimaryIPAddress {
            IPAddress      = "$($node.IPAddress)/$($node.PrefixLength)"
            InterfaceAlias = $node.InterfaceAlias
            AddressFamily  = $node.AddressFamily
        }

        DnsServerAddress DnsServerAddress {
            Address        = $node.DnsServerAddress
            InterfaceAlias = $node.InterfaceAlias
            AddressFamily  = $node.AddressFamily
        }

        Script DisableServerManager {
            GetScript = {
                return Get-ScheduledTask -TaskName Servermanager -ErrorAction SilentlyContinue
            }
            TestScript = {
                $x = Get-ScheduledTask -TaskName Servermanager -ErrorAction SilentlyContinue
                if($null -eq $x) { return $true }
                else {
                    return $x.State -eq 'Disabled'
                }
            }
            SetScript = {
                Get-ScheduledTask -TaskName Servermanager | Disable-ScheduledTask
            }
        }
    }

    node $AllNodes.Where({ $_.Roles -contains 'DC'}).NodeName {

        WindowsFeatureSet AD {
            Name   = @('AD-Domain-Services', 'RSAT-ADDS-Tools', 'DNS', 'RSAT-DNS-Server')
            Ensure = 'Present'
        }

        Computer Name {
            Name = $node.NodeName
        }

        ADDomain $domainName {
            DomainName                    = $domainName
            Credential                    = $localUser
            SafeModeAdministratorPassword = $localUser
            ForestMode                    = 'WinThreshold'
            DependsOn                     = @('[Computer]Name','[WindowsFeatureSet]AD')
        }
    }

    node $AllNodes.Where({ $_.Roles -contains 'MS'}).NodeName {

        WaitForADDomain $domainName {
            DomainName  = $domainName
            Credential  = $domainuser
            WaitTimeout = 1800 # 30 minutes
        }

        Computer JoinDomain {
            Name       = $node.NodeName
            DomainName = $domainName
            Credential = $domainuser
            DependsOn  = @('[DnsServerAddress]DnsServerAddress',"[WaitForADDomain]$domainName")
        }
    }

    node $AllNodes.Where({ $_.Roles -contains 'WEB'}).NodeName {
        WindowsFeature IIS {
            Name   = 'Web-Server'
            Ensure = 'Present'
        }

        Script InstallDotNet {
            GetScript = {
                $x = Get-Item "$env:ProgramFiles\dotnet\dotnet.exe" -ErrorAction 'SilentlyContinue'
                return $x
            }
            TestScript = {
                $x = Get-Item "$env:ProgramFiles\dotnet\dotnet.exe" -ErrorAction 'SilentlyContinue'
                return $x -ne $null
            }
            SetScript = {
                $process = Start-Process -FilePath 'C:\Resources\aspdotnet7runtime.exe' -ArgumentList "/q /norestart" -Wait -NoNewWindow -PassThru
                Write-Host "Installer exited with code '$($process.ExitCode)'"
            }
        }
    }

    node $AllNodes.Where({ $_.Roles -contains 'DB'}).NodeName {
        $instanceName = 'presentationdb'
        SqlSetup '2019Dev' {
            Action              = 'Install'
            SourcePath          = 'C:\Resources\sqlserver'
            Features            = 'SQLENGINE'
            InstanceName        = $instanceName
            SQLCollation        = 'SQL_Latin1_General_Cp1_CI_AS'
            SecurityMode        = 'SQL'
            SAPwd               = $domainuser
            SQLSysAdminAccounts = @($domainuser.UserName)
            # DependsOn           = @('[Computer]JoinDomain')
        }

        SqlProtocol 'EnableTcpIp' {
            InstanceName = $instanceName
            ProtocolName = 'TcpIp'
            Enabled      = $true
            DependsOn    = @('[SqlSetup]2019Dev')
        }

        SqlProtocolTcpIP 'ChangeIP2' {
            InstanceName   = $instanceName
            IpAddressGroup = 'IP2'
            Enabled        = $true
            TcpPort        = ''
            DependsOn      = @('[SqlProtocol]EnableTcpIp')
        }

        SqlProtocolTcpIP 'ChangeIPAll' {
            InstanceName   = $instanceName
            IpAddressGroup = 'IPAll'
            TcpPort        = '1433'
            DependsOn      = @('[SqlProtocol]EnableTcpIp')
        }

        Firewall SqlServer {
            Name        = 'Sql Server'
            Description = 'Incoming Rule for Sql Server'
            Ensure      = 'Present'
            LocalPort   = @('1433')
            Direction   = 'Inbound'
            Action      = 'Allow'
            Enabled     = 'True'
            Protocol    = 'TCP'
            Profile     = ('Public','Domain', 'Private')
        }
    }

    node $AllNodes.Where({ $_.Roles -contains 'SMB'}).NodeName {
        WindowsFeature FileServer {
            Name   = 'FS-FileServer'
            Ensure = 'Present'
        }

        foreach ($disk in $node.Disks) {
            WaitForDisk $disk.DiskNumber {
                DiskId           = $disk.DiskNumber
                RetryIntervalSec = 60
                RetryCount       = 60
            }
            Disk $disk.DiskNumber {
                DiskId      = $disk.DiskNumber
                DriveLetter = $disk.DriveLetter
                FSFormat    = $disk.FSFormat
                DependsOn   = "[WaitForDisk]$($disk.DiskNumber)"
            }
            WaitForVolume $disk.DriveLetter {
                DriveLetter = $disk.DriveLetter
                RetryIntervalSec = 60
                RetryCount       = 60
            }
            SmbShare $disk.SmbSharename {
                Name = $disk.SmbSharename
                Path = "$($disk.DriveLetter):\"
                FullAccess = @($domainuser.Username, "$domainName\$fsfTestUser")
                DependsOn = @("[WaitForADDomain]$domainName", "[WaitForVolume]$($disk.DriveLetter)")
            }
        }
    }
}