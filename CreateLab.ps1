#requires -RunAsAdministrator

Import-Module Lability

$configurationData = & "$PSScriptRoot\LabConfigurationData.ps1"

$password = ConvertTo-SecureString -String $configurationData.NonNodeData.Password -AsPlainText -Force
$username = $configurationData.NonNodeData.Username
$credential = New-Object System.Management.Automation.PSCredential ($username, $password)

. "$PSScriptRoot\LabConfiguration.ps1"
& LabConfiguration -ConfigurationData $configurationData -OutputPath "$PSScriptRoot\publish"

Start-LabConfiguration -ConfigurationData $configurationData `
    -Password $password `
    -Path "$PSScriptRoot\publish" `
    -IgnorePendingReboot -NoSnapshot `
    -Verbose

Start-Lab -ConfigurationData $configurationData -Verbose

Wait-Lab -ConfigurationData $configurationData `
    -PreferNodeProperty 'IPAddress' `
    -Credential $credential `
    -RetryCount 90 `
    -ErrorAction 'SilentlyContinue' `
    -Verbose